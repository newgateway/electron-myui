import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/layout'),
    children: [
      {
        path: '',
        component: () => import('@/views/index'),
        meta: {
          tab: '导航菜单一'
        }
      },
      {
        path: '/page',
        component: () => import('@/views/page'),
        meta: {
          tab: '导航菜单二'
        }
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
